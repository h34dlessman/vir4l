class Jiggo {
  regexizePhoneNumber(number) {
    let phone = number;
    if (number.indexOf("00") == 0) {
      phone = "+" + number.substring(2);
    }
    return phone;
  }

  sorta(a, b) {
    return b.data().up - b.data().down - (a.data().up - a.data().down) == 0
      ? b.data().up - a.data().up
      : b.data().up - b.data().down - (a.data().up - a.data().down);
  }
}

let jiggo = new Jiggo();
export default jiggo;
