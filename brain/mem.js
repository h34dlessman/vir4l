class Grr {
  constructor() {
    this.votes = [];
  }
  deVote(id) {
    let index = this.votes.findIndex(vote => vote.id == id);
    return index == undefined || index == -1 ? 0 : this.votes[index].vote;
  }
  frashenVotes(userdoc) {
    console.log("GRRR NEW VOOOTEESSSssSs");
    if (!userdoc) {
      return;
    }
    this.votes = userdoc.data().votes;
    console.log(this.votes);
  }
  preprocessVote(vote, id) {
    console.log("GRR preprocess vote")
    const index = this.votes.findIndex(vote => vote.id === id);
    const memVote =
      index === undefined || index === -1 ? 0 : this.votes[index].vote;

    if (memVote === 0) {
      console.log("GRR newvote")
      const voteObj = {
        id,
        vote
      };
      if (index === undefined || index === -1) {
        this.votes.push(voteObj);
      } else {
        this.votes[index] = voteObj;
      }

      return {down: vote==-1?1:0, up: vote==1?1:0};

    } else if (memVote === vote) {
      console.log("GRR unvote")
      this.votes.splice(index, 1);

      return {down: vote==-1?-1:0, up: vote==1?-1:0};

    } else {
      console.log("GRR revote")
      this.votes[index].vote = vote;

      return {down: vote==-1?1:-1, up: vote==1?1:-1};
    }
  }
}

var grr = new Grr();
export default grr;
