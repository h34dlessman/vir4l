import tenna from "./tenna.js";
import grr from "./mem.js";

class Brain {
  a(x) {
    console.log("brain got: " + x);
  }

  constructor() {
    this.b = "a";
    tenna.votesCallback = grr.frashenVotes;
  }
}

let brain = new Brain();

export default brain;
export { tenna, grr };
