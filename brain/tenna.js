import firebase from "@react-native-firebase/app";
import analytics from "@react-native-firebase/analytics";
import auth from "@react-native-firebase/auth";
import messaging from "@react-native-firebase/messaging";
import firestore from "@react-native-firebase/firestore";
import functions from "@react-native-firebase/functions";

import jiggo from "./jiggo.js";

// TODO(you): import any additional firebase services that you require for your app, e.g for auth:
//    1) install the npm package: `yarn add @react-native-firebase/auth@alpha` - you do not need to
//       run linking commands - this happens automatically at build time now
//    2) rebuild your app via `yarn run run:android` or `yarn run run:ios`
//    3) import the package here in your JavaScript code: `import '@react-native-firebase/auth';`
//    4) The Firebase Auth service is now available to use here: `firebase.auth().currentUser`

class Tenna {
  constructor() {
    this.confirmation = null;
    this.votesCallback = () => null;
    this.uid = "";
    //Cloud Messaging
    registerAppWithFCM();
    requestPermission();
  }

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    CONVERSATION
  async getVids() {
    const videos = await firestore()
      .collection("video")
      .orderBy("created", "desc")
      .get();
    console.log(videos);

    return videos.docs;
  }

  async titulate(titles) {
    try {
      const title = await functions().httpsCallable("titulate")({
        titleIDs: titles.map(title => title.id)
      });
      return title.data;
    } catch (e) {
      console.error(e);
    }
    return "";
  }

  async getVir4lz() {
    let today = new Date();
    today.setDate(today.getDate());
    today.setHours(0, 0, 0, 0);
    const vir4lz = await firestore()
      .collection("video")
      .orderBy("created", "desc")
      .where("created", ">=", today)
      .get();
    let sorted = vir4lz.docs.sort(jiggo.sorta).splice(0, 3);
    return sorted;
  }

  async upvote(id) {
    this.vote(id, 1);
  }

  async downvote(id) {
    this.vote(id, -1);
  }

  async vote(id, vote) {
    try {
      const success = await functions().httpsCallable("vote")({
        id,
        vote,
        uid: this.uid
      });

      if (success) {
        console.log("downvoted");
      } else {
        console.warn("Woops, looks like something went wrong!");
      }
    } catch (e) {
      console.error(e);
    }
  }

  async voteTitle(videoID, title) {
    const voteID = this.uid + videoID;

    const voteSnapshot = await firestore()
      .collection("titleVote")
      .doc(voteID)
      .get();

    const preVote = voteSnapshot.data();
    let unvoteTitleID = "";
    if (preVote !== undefined) {
      unvoteTitleID = preVote.titleID;
    }
    if (unvoteTitleID == title.id) {
      return;
    }

    await functions().httpsCallable("voteTitle")({
      titleID: title.id,
      unvoteTitleID
    });

    await firestore()
      .collection("titleVote")
      .doc(voteID)
      .set({
        titleID: title.id
      });
  }

  async addVideo(title, id) {
    try {
      const success = await functions().httpsCallable("addVid")({
        title,
        id
      });

      if (success) {
        console.log("vid added");
      } else {
        console.warn("Woops, looks like something went wrong!");
      }
    } catch (e) {
      console.error(e);
    }
  }

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    FRASHERS
  registerToAuthChanges(callback) {
    firebase.auth().onAuthStateChanged(user => {
      this.user = user;
      if (user != null && user != undefined && user.uid) {
        this.frashVotes(user.uid);
        //WARNING: unsubscribe, dont double subscribe
        this.uid = user.uid;
      } else {
        this.uid = null;
      }
      callback(user);
    });
  }
  async setUpVideoListener(callback) {
    console.log("setting up videos listener");
    const documentSnapshot = await firestore()
      .collection("video")
      .onSnapshot(callback);
  }

  async frashVotes(uid) {
    const documentSnapshot = await firestore()
      .collection("user")
      .doc(uid)
      .onSnapshot(this.votesCallback);
  }

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    Auth
  async logMeIn(phone) {
    phone = jiggo.regexizePhoneNumber(phone);
    console.log("tenna - loginWithPhone: " + phone);
    const confirmation = await auth().signInWithPhoneNumber(phone);
    return confirmation;
  }

  async confirmLogin(confirmation, code) {
    try {
      await confirmation.confirm(code); // User entered code
      // Successful login - onAuthStateChanged is triggered
    } catch (e) {
      console.error(e); // Invalid code
    }
  }

  async logOut() {
    //auth().currentUser.delete();
    auth().signOut();
  }
}

//Cloud MEssaging
async function registerAppWithFCM() {
  await messaging().registerForRemoteNotifications();
}

async function requestPermission() {
  const granted = messaging().requestPermission();

  if (granted) {
    console.log("User granted messaging permissions!");
  } else {
    console.log("User declined messaging permissions :(");
  }
}

let tenna = new Tenna();
export default tenna;
