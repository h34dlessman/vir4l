import React, { Component, useState, useEffect } from "react";
import { Platform, StyleSheet, Text, SafeAreaView, View } from "react-native";

import firebase from "@react-native-firebase/app";
import analytics from "@react-native-firebase/analytics";
import auth from "@react-native-firebase/auth";
import messaging from "@react-native-firebase/messaging";
import firestore from "@react-native-firebase/firestore";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "./views/Home";
import Auth from "./views/Auth";
import AddScreen from "./views/AddScreen";
import Vir4lization from "./views/Vir4lization";

import brain, { tenna } from "./brain";

const firebaseCredentials = Platform.select({
  ios: "https://invertase.link/firebase-ios",
  android: "https://invertase.link/firebase-android"
});

type Props = {};

const Stack = createStackNavigator();

export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      user: null
    };
    tenna.registerToAuthChanges(user => {
      this.onAuthStateChanged(user);
    });
  }

  onAuthStateChanged(user) {
    if (!user || user == undefined) {
    } else if (!this.state.loggedIn) {
      this.setState({ loggedIn: true });
    }
    console.log("auth change");
    console.log(user);
    this.setState({ user });
  }

  render() {
    const { loggedIn } = this.state;
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={loggedIn ? Home : Auth}
            options={{
              title: "ViR4l",
              headerStyle: { backgroundColor: "#232323" },
              headerTintColor: "#fff"
            }}
          />
          <Stack.Screen
            name="AddScreen"
            component={AddScreen}
            options={{ title: "💥" }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

//Analytics
async function onProductView() {
  await analytics().logEvent("test_event", {
    id: "123456789",
    color: "red",
    via: "ProductCatalog"
  });
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
