import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  TextInput
} from "react-native";
import { tenna, grr } from "../brain";
import { Button } from "./usable/";

import { styles } from "./_styles";

export default class Auth extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      phoneAuth: false,
      phoneNumber: "",
      confirmation: null
    };
  }

  async commitNumber() {
    const { phoneNumber } = this.state;
    const confirmation = await tenna.logMeIn(phoneNumber);
    this.setState({ confirmation });
  }

  async confirmNumber() {
    const { confirmationCode, confirmation } = this.state;
    await tenna.confirmLogin(confirmation, confirmationCode);
  }

  render() {
    const { phoneAuth } = this.state;
    return (
      <SafeAreaView style={{ ...styles.mainFlex, ...styles.flexCenter }}>
        {phoneAuth ? this.renderPhoneAuth() : this.renderOptions()}
        <Text style={styles.medium}>Log in homes</Text>
      </SafeAreaView>
    );
  }

  renderPhoneAuth() {
    const { phoneNumber, confirmationCode, confirmation } = this.state;
    return (
      <View>
        <View style={{ height: "15%" }} />
        <Text style={{ ...styles.big, ...styles.bigShaqo }}>
          {confirmation ? "Enter ConfirmationCode" : "Enter Phone numberooo"}
        </Text>
        <View style={{ height: "10%" }} />
        <TextInput
          value={confirmation ? confirmationCode : phoneNumber}
          onChangeText={
            confirmation
              ? confirmationCode => this.setState({ confirmationCode })
              : phoneNumber => this.setState({ phoneNumber })
          }
          placeholder={confirmation ? "123456" : "+1 xxx xxxxxxxx"}
          style={{ ...styles.big }}
        />
        <View style={{ height: "5%" }} />
        <Button
          onPress={() => {
            confirmation ? this.confirmNumber() : this.commitNumber();
          }}
        >
          <Text
            style={{
              ...styles.title,
              ...(confirmation ? styles.fire : styles.green)
            }}
          >
            {confirmation ? "Pop IN!" : "Confirm"}
          </Text>
        </Button>
        <View style={{ height: "7.5%" }} />
        <Button
          onPress={() => {
            this.setState({ phoneAuth: false });
          }}
        >
          <Text style={styles.medium}>back</Text>
        </Button>
      </View>
    );
  }

  renderOptions() {
    return (
      <View>
        <Text style={{ ...styles.big, ...styles.bigShaqos }}>
          Authenticate with
        </Text>
        <View style={{ height: "10%" }} />
        <Button
          onPress={() => {
            this.setState({ phoneAuth: true });
          }}
        >
          <Text style={{ ...styles.big, ...styles.green }}>Phone Number</Text>
        </Button>
        <View style={{ height: "5%" }} />
        <Button
          onPress={() => {
            this.setState({ authenticator: 2 });
          }}
        >
          <Text style={{ ...styles.big, ...styles.google }}>
            Google Account
          </Text>
        </Button>
      </View>
    );
  }
}

const style = StyleSheet.create({});

Auth.propTypes = {};
Auth.defaultProps = {};
