import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, FlatList } from "react-native";
import VideoCard from "./usable/VideoCard.js";
import { tenna, grr } from "../brain";
import Add from "./usable/Add.js";
import { Button } from "./usable";

export default class Vir4lization extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      videos: [],
      refreshing: true
    };
    tenna.getVids().then(videos => {
      this.receivedVideos(videos);
    });
  }

  receivedVideos(videos) {
    this.setState({
      videos,
      refreshing: false
    });
  }

  async refresh() {
    this.setState({ refreshing: true });
    const videos = await tenna.getVids();
    this.receivedVideos(videos);
  }

  render() {
    const { videos, refreshing } = this.state;
    const { navigation } = this.props;
    return (
      <View style={style.container}>
        <Button onPress={tenna.logOut}>
          <Text>logout</Text>
        </Button>
        <FlatList
          style={{ width: "100%", height: "100%" }}
          data={videos}
          renderItem={this.renderVideoCard}
          keyExtractor={item => item.id}
          refreshing={refreshing}
          onRefresh={() => this.refresh()}
        />
        <View style={style.naVbTnCont}>
          <Button onPress={() => navigation.pop()}>
            <Text styles={{ fontSize: 62 }}>⭐️</Text>
          </Button>
        </View>
      </View>
    );
  }

  renderVideoCard({ item }) {
    return (
      <VideoCard
        upAction={id => {
          tenna.upvote(id);
        }}
        downAction={id => {
          tenna.downvote(id);
        }}
        video={{ id: item.id, ...item.data() }}
      />
    );
  }
}

const style = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fffef3"
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16
  },
  title: {
    fontSize: 32
  },
  naVbTnCont: {
    position: "absolute",
    bottom: 0,
    height: 80,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  }
});

Vir4lization.propTypes = {};
Vir4lization.defaultProps = {};
