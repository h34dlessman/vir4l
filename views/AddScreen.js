import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
  Alert,
  ActivityIndicator
} from "react-native";
import Button from "./usable/Button.js";
import { tenna, grr } from "../brain";
import VideoCard from "./usable/VideoCard.js";

import { styles } from "./_styles";

export default class AddScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      link: "",
      linkId: "",
      loading: false
    };
  }

  pushIt() {
    const { title, linkId } = this.state;
    if (title && linkId) {
      this.setState({
        sending: true
      });
      tenna.addVideo(title, linkId).then(() => this.props.navigation.pop());
    } else {
      Alert.alert("enter title and valid link 👹");
    }
  }

  inputLink(link) {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = link.match(regExp);
    var linkId = match && match[7].length == 11 ? match[7] : "";
    this.setState({
      link,
      linkId
    });
  }

  render() {
    const { title, link, sending, linkId } = this.state;
    return (
      <View style={styles.main}>
        <View style={styles.horizontal}>
          <Text>title</Text>
          <TextInput
            style={style.input}
            value={title}
            onChangeText={title => this.setState({ title })}
          />
        </View>
        <View style={styles.horizontal}>
          <Text>link</Text>
          <TextInput
            style={style.input}
            value={link}
            onChangeText={link => this.inputLink(link)}
          />
        </View>
        <Button onPress={() => !sending && this.pushIt()}>
          <Text>submit</Text>
        </Button>
        <View style={style.preview}>{this.renderPreview()}</View>
        {sending && this.renderSending()}
      </View>
    );
  }

  renderSending() {
    return <ActivityIndicator size="large" color="#00ff00" />;
  }

  renderPreview() {
    const { title, link, linkId } = this.state;
    return (
      <VideoCard
        video={{
          id: "",
          link: linkId,
          title: title,
          up: 0,
          down: 0
        }}
      />
    );
  }
}

const style = StyleSheet.create({
  input: {
    width: "70%",
    height: 30,
    backgroundColor: "lightgray"
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16
  },
  preview: {
    height: 300,
    width: "70%"
  },
  sending: {
    position: "absolute",
    top: "50%",
    left: "50%",
    width: 100,
    height: 100
  }
});
