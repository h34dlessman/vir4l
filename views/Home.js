import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  StatusBar
} from "react-native";
import VideoCard from "./usable/VideoCard.js";
import { tenna, grr } from "../brain";
import Add from "./usable/Add.js";
import Button from "./usable/Button.js";

import { styles } from "./_styles";

export default class Home extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      videos: [],
      refreshing: true,
      vir4lization: false,
      loading: false
    };
    tenna.getVir4lz().then(videos => {
      this.receivedVideos(videos);
    });
  }

  receivedVideos(videos) {
    this.setState({
      videos,
      refreshing: false,
      loading: false
    });
  }

  async refresh() {
    this.setState({ refreshing: true });
    const { vir4lization } = this.state;
    const videos = await (vir4lization ? tenna.getVids() : tenna.getVir4lz());
    this.receivedVideos(videos);
  }

  async toggleVir4l() {
    const { vir4lization } = this.state;
    this.setState({ vir4lization: !vir4lization, loading: true });
    const videos = await (!vir4lization ? tenna.getVids() : tenna.getVir4lz());
    this.receivedVideos(videos);
  }

  render() {
    const { videos, refreshing, vir4lization, loading } = this.state;
    return (
      <View style={styles.main}>
        <StatusBar barStyle="light-content" />
        <Button onPress={tenna.logOut}>
          <Text>logout</Text>
        </Button>
        <FlatList
          style={{ width: "100%", height: "100%" }}
          data={videos}
          renderItem={item => this.renderVideoCard(item)}
          keyExtractor={item => item.id}
          refreshing={refreshing}
          onRefresh={() => this.refresh()}
        />
        {vir4lization ? this.renderVir4lBtnz() : this.renderStarBtnz()}
        {loading && this.renderLoading()}
      </View>
    );
  }

  renderVir4lBtnz() {
    const { navigation } = this.props;
    return (
      <View style={style.naVbTnCont}>
        <Button onPress={() => this.toggleVir4l()}>
          <Text style={{ fontSize: 40 }}>⭐️</Text>
        </Button>
        <Button onPress={() => navigation.navigate("AddScreen")}>
          <Text style={{ fontSize: 20 }}>💥</Text>
        </Button>
      </View>
    );
  }

  renderStarBtnz() {
    return (
      <View style={style.naVbTnCont}>
        <Button onPress={() => this.toggleVir4l()}>
          <Text style={{ fontSize: 45 }}>🔥</Text>
        </Button>
      </View>
    );
  }

  renderVideoCard({ item }) {
    return (
      <VideoCard
        vir4lization={this.state.vir4lization}
        upAction={id => {
          tenna.upvote(id);
        }}
        downAction={id => {
          tenna.downvote(id);
        }}
        video={{ id: item.id, ...item.data() }}
      />
    );
  }

  renderLoading() {
    return (
      <ActivityIndicator
        size="large"
        color="#00ff00"
        style={{ position: "absolute" }}
      />
    );
  }
}

const style = StyleSheet.create({
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16
  },
  naVbTnCont: {
    position: "absolute",
    bottom: 0,
    height: 100,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  }
});

Home.propTypes = {};
Home.defaultProps = {};
