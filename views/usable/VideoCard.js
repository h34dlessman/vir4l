import React, { Component } from "react";
import { Animated, View, Text, FlatList } from "react-native";
import YouTube from "react-native-youtube";
import { Button } from "./";

import { grr, tenna } from "../../brain";

import { styles } from "../_styles";
var { vw, vh, vmin, vmax } = require("react-native-viewport-units");

const defaultHeight = 115 * vw;

class VideoCard extends Component {
  static propTypes = {};

  static defaultProps = {
    video: {
      id: "http://link.de",
      title: "Title",
      up: 0,
      down: 0
    },
    upAction: () => null,
    downAction: () => null
  };

  constructor(props) {
    super(props);
    const { id, up, down, titles } = props.video;
    this.state = {
      id,
      title: "",
      up,
      down,
      titles,
      loadedTitles: [],
      votingTitle: false,
      animatedWidth: new Animated.Value(100 * vw - 24),
      animatedHeight: new Animated.Value(115 * vw)
    };
    this.preloadTitles();
    if (titles && titles != undefined) {
      this.titulization().then(title => this.setState({ title }));
    }
  }

  toggleAnimation = () => {
    if (!this.state.votingTitle) {
      Animated.timing(this.state.animatedWidth, {
        toValue: 100 * vw - 24,
        timing: 5
      }).start(() => {
        this.setState({ votingTitle: true });
      });
      Animated.timing(this.state.animatedHeight, {
        toValue: defaultHeight + 50,
        timing: 5
      }).start();
    } else {
      Animated.timing(this.state.animatedWidth, {
        toValue: 100 * vw - 24,
        timing: 5
      }).start(this.setState({ votingTitle: false }));
      Animated.timing(this.state.animatedHeight, {
        toValue: defaultHeight,
        timing: 5
      }).start();
    }
  };

  async preloadTitles() {
    const { titles } = this.state;
    const promises = [];
    for (var i = 0; i < titles.length; i++) {
      promises.push(titles[i].get());
    }
    const loadedTitles = await Promise.all(promises);
    this.setState({ loadedTitles });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { id, title } = nextProps.video;

    if (title !== prevState.title || id !== prevState.id) {
      return title == undefined ? { id } : { id, title };
    }
    return null;
  }

  async titulization() {
    const { titles, title } = this.state;
    if (
      (title && title != "") ||
      (!titles && titles == undefined && titles.length <= 0)
    ) {
      return title;
    }

    const tit = await tenna.titulate(titles);
    console.log("tit" + tit);
    return tit;
  }

  processVoteChanges(vote, id) {
    const { up, down } = this.state;
    const { upAction, downAction } = this.props;

    vote == 1 ? upAction(id) : downAction(id);
    let changes = grr.preprocessVote(vote, id);
    this.setState({ up: up + changes.up, down: down + changes.down });
  }

  voteTitle(title) {
    const { id } = this.state;
    tenna.voteTitle(id, title);
    this.toggleAnimation();
  }

  render() {
    const { id, title, up, down, titles, votingTitle } = this.state;

    const animatedStyle = {
      width: this.state.animatedWidth,
      height: this.state.animatedHeight
    };
    return (
      <Animated.View style={style.outer}>
        <Animated.View style={[style.container, animatedStyle]}>
          {this.renderTitle()}
          <YouTube
            videoId={id} // The YouTube video IDplay
            play={false} // control playback of video with true/falsefullscreen
            fullscreen={false} // control whether the video should play in fullscreen or inlineloop
            loop={false} // control whether the video should loop when endedonReady
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onError={e => this.setState({ error: e.error })}
            style={{
              alignSelf: "stretch",
              height: defaultHeight - 120,
              backgroundColor: "#000"
            }}
          />
          <View>
            <View style={styles.horizontal}>
              <Button
                style={style.updownBtn}
                onPress={() => this.processVoteChanges(1, id)}
              >
                <Text style={style.updownBtnText}>⭐️</Text>
              </Button>
              <Button
                style={style.updownBtn}
                onPress={() => this.processVoteChanges(-1, id)}
              >
                <Text style={style.updownBtnText}>💩</Text>
              </Button>
            </View>
            <View style={styles.horizontal}>
              <Text style={style.updownText}>{up}</Text>
              <Text style={style.updownText}>{down}</Text>
            </View>
          </View>
        </Animated.View>
      </Animated.View>
    );
  }

  renderTitle() {
    const { title, votingTitle, loadedTitles } = this.state;
    const { vir4lization } = this.props;
    if (!votingTitle) {
      return (
        <Button
          onPress={() => {
            vir4lization ? this.toggleAnimation() : {};
          }}
        >
          <View
            style={{
              height: 35,
              width: "85%",
              alignItems: "center",
              justifyContent: "center",
              alignItems: "center",
              display: "flex"
            }}
          >
            <Text
              style={{
                ...{
                  color: "#fff"
                },
                ...styles.title
              }}
            >
              {title}
            </Text>
          </View>
        </Button>
      );
    }

    return (
      <FlatList
        style={style.titleList}
        renderItem={({ item }) => {
          return (
            <Button onPress={() => this.voteTitle(item)}>
              <Text
                style={{
                  ...{
                    color: "#fff",
                    width: "100%",
                    textAlign: "center"
                  },
                  ...styles.standard
                }}
              >
                {item.data().title}
              </Text>
            </Button>
          );
        }}
        data={loadedTitles}
        keyExtractor={item => item.id}
      />
    );
  }
}

const style = {
  outer: {
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#121212",
    marginBottom: 15,
    borderRadius: 8
  },
  containerBig: {
    width: "100%",
    height: 450
  },
  updownText: {
    width: 80,
    textAlign: "center",
    marginTop: 3,
    color: "#fff"
  },
  updownBtn: {},
  updownBtnText: {
    width: 80,
    textAlign: "center",
    fontSize: 40
  },
  titleList: {
    width: "100%",
    height: 70
  }
};

export default VideoCard;
