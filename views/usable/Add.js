import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image
} from "react-native";
import { tenna, grr } from "../../brain";
import { Button } from "./";

export default class Add extends Component<Props> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Button onPress={this.props.showAdder}>
          <Image source={require("../../images/lelit.png")} />
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    width: 30,
    height: 30,
    bottom: 40,
    right: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});
