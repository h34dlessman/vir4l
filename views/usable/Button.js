import React, { Component } from "react";
import { TouchableWithoutFeedback, Text } from "react-native";

class Button extends Component {
  static propTypes = {};

  static defaultProps = {
    onPress: () => {},
    style: {}
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={this.props.onPress}
        style={this.props.style}
      >
        {this.props.children}
      </TouchableWithoutFeedback>
    );
  }
}

export default Button;
