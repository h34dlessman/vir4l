import React, { Component } from "react";
import { View, FlatList } from "react-native";

export default class PopUpList extends Component {
  constructor(props) {
    super(props);
    const { items, show } = this.props;
    this.state = { items, show };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return nextProps;
  }

  render() {
    const { items, show } = this.state;
    const { renderItem } = this.props;

    return (
      <View
        style={style.main}
        animationType="slide"
        visible={show}
        transparent={false}
      >
        <FlatList
          style={{ ...this.props.style, ...style.list }}
          data={items}
          renderItem={renderItem}
          keyExtractor={item => item}
        />
      </View>
    );
  }
}

const style = {
  main: {
    width: 100,
    height: 300,
    backgroundColor: "white",
    position: "absolute"
  },
  list: {}
};
