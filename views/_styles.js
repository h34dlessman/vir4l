export const styles = {
  horizontal: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  main: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#343434"
  },
  mainFlex: {
    display: "flex",
    width: "100%",
    height: "100%",
    alignItems: "center",
    backgroundColor: "#343434"
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    FONTS
  big: {
    fontSize: 26,
    color: "white",
    textAlign: "center"
  },
  bigA: {
    fontSize: 30,
    color: "white",
    textAlign: "center"
  },
  title: {
    fontSize: 20,
    color: "white",
    textAlign: "center"
  },
  medium: {
    fontSize: 16,
    color: "white",
    textAlign: "center"
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    COLORS
  green: {
    color: "#87de23"
  },
  fire: {
    color: "#f13d19"
  },
  google: {
    color: "#ff5722"
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    SHADOWS
  bigShaqo: {
    textShadowColor: "#000",
    textShadowOffset: {
      width: 0,
      height: 1
    },
    textShadowOpacity: 0.22,
    textShadowRadius: 2.22,

    elevation: 3
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    POSITIONING

  flexAround: {
    justifyContent: "space-around"
  },
  flexCenter: {
    justifyContent: "center"
  }
};
